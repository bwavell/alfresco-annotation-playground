# alfresco-annotation-playground

## WARNING

This code was developed as a proof of concept. I do not recommend using any of this
as is in production without further investigation, testing and boundary condition
remediation.

I'm particularly concerned about the hack I had to use in MyWebscriptStore to allow
my dynamic webscript store to be injected into the webscripts.searchpath bean. 

## License

Apache 2.0
