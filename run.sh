#!/bin/bash
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.7.0_51.jdk/Contents/Home/"
export MAVEN_OPTS="-Xms1024m -Xmx4096m -XX:PermSize=1024m"
mvn clean integration-test -Pamp-to-war -DskipTests=true
