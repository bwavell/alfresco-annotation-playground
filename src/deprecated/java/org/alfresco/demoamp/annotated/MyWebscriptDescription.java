package org.alfresco.demoamp.annotated;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.extensions.webscripts.DescriptionExtension;
import org.springframework.stereotype.Component;

//@Component("webscriptdesc.org.alfresco.demoamp.annotated.mywebscript.get")
public class MyWebscriptDescription implements DescriptionExtension
{
	@Override
	public Map<String, Serializable> parseExtensions(String serviceDescPath, InputStream servicedesc) {
		Map<String, Serializable> desc = new HashMap<String, Serializable>();
		desc.put("id", "mywebscript");
		desc.put("shortname", "MyWebscript");
		desc.put("description", "Logs a raw debug message");
		desc.put("url", "/mywebscript");
		desc.put("authentication", "user");
		return desc;
	}

}
