package org.alfresco.demoamp.annotated;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

import org.alfresco.i18n.ResourceBundleBootstrapComponent;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.extensions.surf.util.I18NUtil;
import org.springframework.stereotype.Component;

@Component("add-aspect")
public class MyAction extends AbstractAnnotatedAction 
{
    private static final Log log = LogFactory.getLog(MyAction.class);
    public static final String NAME = "add-aspect";
    public static final String PARAM_ASPECT_NAME = "aspect-name";

	@Autowired @Qualifier("NodeService")
	private NodeService nodeService;

	
	@Override
	protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
		if (this.nodeService.exists(actionedUponNodeRef)) {
			QName aspectQName = (QName)action.getParameterValue(PARAM_ASPECT_NAME);
		    this.nodeService.addAspect(actionedUponNodeRef, aspectQName, null);
		}
	}

	@Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
	    paramList.add(
	    	new ParameterDefinitionImpl(                        // Create a new parameter defintion to add to the list
	            PARAM_ASPECT_NAME,                              // The name used to identify the parameter
	            DataTypeDefinition.QNAME,                       // The parameter value type
	            true,                                           // Indicates whether the parameter is mandatory
	            getParamDisplayLabel(PARAM_ASPECT_NAME)));      // The parameters display label
	}
	
	@Override
	protected void registerResourceBundles(ResourceBundleBootstrapComponent actionResourceBundles) {
		//actionResourceBundles.setResourceBundles(Arrays.asList("org.alfresco.demoamp.annotated.add-aspect-action-messages"));
		ResourceBundle bundle = ResourceBundle.getBundle("org.alfresco.demoamp.annotated.MyActionResource");
	}

}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
