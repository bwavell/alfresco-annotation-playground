package org.alfresco.demoamp.annotated;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component("my-bean")
public class MyBean implements ApplicationContextAware, InitializingBean {

	private static final Log log = LogFactory.getLog(MyBean.class);

	@Value("${dir.root}")
	private String dirRoot;
	private ApplicationContext ctx;

	@PostConstruct
	public void init() {
		log.debug("POST CONSTRUCTION");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (null != dirRoot) {
			log.debug("DIR ROOT: " + dirRoot);
		} else {
			log.debug("DIR ROOT NOT INJECTED!");
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.debug("CAPTURING APPLICATION CONTEXT");
		ctx = applicationContext;
	}
}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
