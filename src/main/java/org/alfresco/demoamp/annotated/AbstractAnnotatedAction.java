package org.alfresco.demoamp.annotated;

import javax.annotation.PostConstruct;

import org.alfresco.i18n.ResourceBundleBootstrapComponent;
import org.alfresco.repo.action.RuntimeActionService;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.lock.LockService;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public abstract class AbstractAnnotatedAction extends ActionExecuterAbstractBase {

	@Override
	@PostConstruct
	public void init() {
		super.init();
		registerResourceBundles(resourceBundleBootstrapComponent);
	}

	@Override
	@Autowired
	@Qualifier("actionService")
	public void setRuntimeActionService(RuntimeActionService runtimeActionService) {
		super.setRuntimeActionService(runtimeActionService);
	}

	@Override
	@Autowired
	@Qualifier("LockService")
	public void setLockService(LockService lockService) {
		super.setLockService(lockService);
	}

	@Override
	@Autowired
	@Qualifier("nodeService")
	public void setBaseNodeService(NodeService nodeService) {
		super.setBaseNodeService(nodeService);
	}

	@Override
	@Autowired
	@Qualifier("mlAwareNodeService")
	public void setMlAwareNodeService(NodeService mlAwareNodeService) {
		super.setMlAwareNodeService(mlAwareNodeService);
	}

	@Override
	@Autowired
	@Qualifier("dictionaryService")
	public void setDictionaryService(DictionaryService dictionaryService) {
		super.setDictionaryService(dictionaryService);
	}

	@Override
	public void setTrackStatus(boolean trackStatus) {
		super.setTrackStatus(false);
	}

	@Autowired
	@Qualifier("actionResourceBundles")
	private ResourceBundleBootstrapComponent resourceBundleBootstrapComponent;
	
	protected abstract void registerResourceBundles(ResourceBundleBootstrapComponent actionResourceBundles);
}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
