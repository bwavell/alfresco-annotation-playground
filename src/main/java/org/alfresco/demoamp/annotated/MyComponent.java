package org.alfresco.demoamp.annotated;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class MyComponent implements InitializingBean, ApplicationContextAware 
{
    private static final Log log = LogFactory.getLog(MyComponent.class);

	private ApplicationContext ctx;
	@Autowired()
	@Qualifier("ServiceRegistry")
	private ServiceRegistry serviceRegistry;
	

	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Got here now");
		log.debug("Got here");
		MyComponent comp = (MyComponent) ctx.getBean("myComponent");
		if (null != comp) {
			System.out.println("My bean is registered");
			log.debug("My bean is registered");
		}
		if (null != serviceRegistry) {
			log.debug("Found the service registry");
			System.out.println("Found the service registry: " + serviceRegistry.getDictionaryService().getType(ContentModel.TYPE_CONTENT).getName());
		}
	}

}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
