package org.alfresco.demoamp.annotated;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.extensions.webscripts.AbstractStore;
import org.springframework.extensions.webscripts.DeclarativeRegistry;
import org.springframework.extensions.webscripts.Description.RequiredAuthentication;
import org.springframework.extensions.webscripts.DescriptionImpl;
import org.springframework.extensions.webscripts.PackageDescriptionDocument;
import org.springframework.extensions.webscripts.SchemaDescriptionDocument;
import org.springframework.extensions.webscripts.ScriptContent;
import org.springframework.extensions.webscripts.ScriptLoader;
import org.springframework.extensions.webscripts.SearchPath;
import org.springframework.extensions.webscripts.Store;
import org.springframework.extensions.webscripts.WebScript;
import org.springframework.stereotype.Component;

import freemarker.cache.TemplateLoader;

@Component("webscripts.mywebscriptstore")
public class MyWebscriptStore extends AbstractStore implements ApplicationContextAware {

	public static final Log log = LogFactory.getLog(MyWebscriptStore.class);

	private Map<String, Object> webscriptAnnotatedBeans = new HashMap<String, Object>();
	private Map<Object, List<Method>> webscriptBeanHandlers = new HashMap<Object, List<Method>>();
	private Map<String, DescriptionImpl> webscriptDescriptions = new HashMap<String, DescriptionImpl>();
	
	private Set<String> webscriptPaths = new HashSet<String>();
	private Set<String> descriptorPaths = new HashSet<String>();
	
	
	@Autowired
	@Qualifier("webscripts.searchpath")
	private SearchPath searchPath;
	
	/**
	 * Add our store to the existing list in the alfresco webscripts.searchpath
	 */
	@PostConstruct
	public void beanInit() {
		Collection<Store> stores = null;
		try {
			// nasty hack because this is not available to us and there is no add search path capability
			Field searchPathField = SearchPath.class.getDeclaredField("searchPath");
			searchPathField.setAccessible(true);
			@SuppressWarnings("unchecked") Collection<Store> tmpStores = (Collection<Store>) searchPathField.get(searchPath);
			stores = tmpStores;
		} catch (NoSuchFieldException e) {
		} catch (SecurityException e) {
		} catch (IllegalAccessException e) {
		}
		if(null != stores && !stores.contains(this)) {
			log.debug("Injecting MyWebscriptStore into SearchPath");
			List<Store> newStores = new ArrayList<Store>();
			newStores.add(this);
			for (Store store : stores) {
				newStores.add(store);
			}
			searchPath.setSearchPath(newStores);
		}
	}

	/**
	 * Initialise Store (called once)
	 */
	@Override
	public void init() {
		log.debug("init");
	}

    /**
     * Determines whether the store actually exists
     * 
     * NOTE: I think this means return true if there are any webscripts available via the store -- BW
     *
     * @return  true => it does exist
     */
	@Override
	public boolean exists() {
		//log.debug("exists");
		return (webscriptPaths.size() > 0);
	}

    /**
     * Gets the base path of the store
     * 
     * NOTE: Unclear if there is any special meaning, but since these webscripts will all be
     * in proc, gonna try just returning an empty base path. -- BW
     *
     * @return base path
     */
	@Override
	public String getBasePath() {
		log.debug("getBasePath");
		return "";
	}

    /**
     * Returns true if this store is considered secure - i.e. on the app-server classpath. Scripts in secure stores can
     * be run under the identity of a declared user (via the runas attribute) rather than the authenticated user.
     *
     * @return true if this store is considered secure
     */
	@Override
	public boolean isSecure() {
		log.debug("isSecure");
		return true;
	}
	
    /**
     * Gets the paths of given document pattern within given path/sub-paths in this store
     *
     * @param path             start path
     * @param includeSubPaths  if true, include sub-paths
     * @param documentPattern  document name, allows wildcards, eg. *.ftl or my*.ftl
     * @return  array of document paths
     */
	@Override
	public String[] getDocumentPaths(String path, boolean includeSubPaths, String documentPattern) throws IOException {
		log.debug("getDocumentPaths: " + path + " sub-paths? " + includeSubPaths + " pattern: " + documentPattern);
		if(PackageDescriptionDocument.DESC_NAME_PATTERN.equals(documentPattern)
				|| SchemaDescriptionDocument.DESC_NAME_PATTERN.equals(documentPattern)) {
			return new String[] {};
		}
		return this.getDocumentPaths(path, documentPattern);
	}

    /**
     * Gets the paths of all Web Script description documents in this store
     *
     * @return array of description document paths
     */
	@Override
	public String[] getDescriptionDocumentPaths() throws IOException {
		log.debug("getDescriptionDocumentPaths");
		performAnnotationProcessing();
		return descriptorPaths.toArray(new String[]{});
	}
	
    private void performAnnotationProcessing() {
    	if (null == applicationContext) return; // bail if we don't have a usable context
    	try {
	    	Map<String, Object> webscriptBeans = applicationContext.getBeansWithAnnotation(Webscript.class);
	    	for (String webscriptBeanName : webscriptBeans.keySet()) {
	    		Object webscriptBean = webscriptAnnotatedBeans.get(webscriptBeanName);
	    		if (null == webscriptBean) {
	    			webscriptBean = webscriptBeans.get(webscriptBeanName);
	    			if (null != webscriptBean) {
	    				webscriptAnnotatedBeans.put(webscriptBeanName, webscriptBean);
	    				webscriptBeanHandlers.put(webscriptBean, new ArrayList<Method>());
	    			}
	    		}
	    		if (null != webscriptBean) {
		    		Webscript webscriptAnnotation = webscriptBean.getClass().getAnnotation(Webscript.class);
	    			List<Method> registeredMethods = webscriptBeanHandlers.get(webscriptBean);
		    		Method[] methods = webscriptBean.getClass().getMethods();
		    		for (Method method : methods) {
		    			if (!registeredMethods.contains(method) && method.isAnnotationPresent(WebscriptHandler.class)) {
		    				String thisClass = webscriptBean.getClass().getSimpleName();
		    				String thisPackage = webscriptBean.getClass().getPackage().getName();
		    				WebscriptHandler handlerAnnotation = method.getAnnotation(WebscriptHandler.class);
		    				String webscriptMethod = handlerAnnotation.method();
		    				String nameOverride = handlerAnnotation.nameoverride();
		    				String beanName = "webscript." + thisPackage + "." + thisClass.toLowerCase() + "." + webscriptMethod.toLowerCase();
		    				String documentPath = thisPackage.replace('.', '/') + "/" + thisClass.toLowerCase() + "." + webscriptMethod.toLowerCase();
		    				if (nameOverride.trim().length() > 0) {
		    					beanName = "webscript." + nameOverride + "." + webscriptMethod.toLowerCase();
		    					documentPath = nameOverride.replace('.',  '/') + "." + webscriptMethod.toLowerCase();
		    				}
		    				if (!webscriptPaths.contains(documentPath)) {
		    					// Create and register a proxy for the webscript
		        				AnnotatedWebscriptProxy proxy = new AnnotatedWebscriptProxy(webscriptBean, method);
		        				applicationContext.getAutowireCapableBeanFactory().autowireBean(proxy);
		        				proxy = (AnnotatedWebscriptProxy) applicationContext.getAutowireCapableBeanFactory().initializeBean(proxy, beanName);
		        				applicationContext.getBeanFactory().registerSingleton(beanName, proxy);
		        				
		        				// Generate descriptor from annotations
			    				String shortName = handlerAnnotation.shortname();
			    				String description = handlerAnnotation.description();
			    				String[] urls = handlerAnnotation.urls();
			    				RequiredAuthentication authentication = handlerAnnotation.authentication();
			    				String runAs = handlerAnnotation.runas();
		        				DescriptionImpl desc = new DescriptionImpl();
		        				desc.setId(beanName);
		        				desc.setShortName(shortName);
		        				desc.setDescription(description);
		        				desc.setUris(urls);
		        				desc.setRequiredAuthentication(authentication);
		        				desc.setRunAs(runAs);
		        				
		        				webscriptPaths.add(documentPath);
		        				descriptorPaths.add(documentPath + DeclarativeRegistry.WEBSCRIPT_DESC_XML);
		        				
		        				registeredMethods.add(method);
		        				webscriptDescriptions.put(documentPath + DeclarativeRegistry.WEBSCRIPT_DESC_XML, desc);
		    				} else {
		    					log.debug("Figure out if we are re-processing the same handler or a different handler with the same vitals.");
		    				}
		    			}
		    		}
	    		}
	    	}
    	} catch (Exception e) {
    		log.debug("Problem performing annotation processing", e);
    	}
	}

	/**
     * Gets the paths of all implementation files for a given Web Script
     *
     * @param script  web script
     * @return  array of implementation document paths
     */
	@Override
	public String[] getScriptDocumentPaths(WebScript script) throws IOException {
		log.debug("getScriptDocumentPaths");
		return new String[]{};
	}

    /**
     * Gets the paths of all documents in this store
     *
     * @return array of all document paths
     */
	@Override
	public String[] getAllDocumentPaths() {
		log.debug("getAllDocumentPaths");
		Set<String> allDocuments = new HashSet<String>(webscriptPaths);
		allDocuments.addAll(descriptorPaths);
		return allDocuments.toArray(new String[]{});
	}

    /**
     * Gets the last modified timestamp for the document.
     *
     * @param documentPath  document path to an existing document
     * @return  last modified timestamp
     *
     * @throws IOException if the document does not exist in the store
     */
	@Override
	public long lastModified(String documentPath) throws IOException {
		log.debug("lastModified");
		throw new IOException("No docs in this store yet");
	}

    /**
     * Determines if the document exists.
     * 
     * @param documentPath
     *            document path
     * @return true => exists, false => does not exist
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
	@Override
	public boolean hasDocument(String documentPath) throws IOException {
		boolean retv = webscriptPaths.contains(documentPath) || descriptorPaths.contains(documentPath);
		if (retv) {
			log.debug("hasDocument: " + documentPath);
		}
		return retv;
	}

    /**
     * Gets a document. Note a raw InputStream to the content is returned and
     * must be closed by the accessing method.
     *
     * @param documentPath  document path
     * @return  input stream onto document
     *
     * @throws IOException if the document does not exist in the store
     */
	@Override
	public InputStream getDocument(String documentPath) throws IOException {
		log.debug("getDocument: " + documentPath);
		if (documentPath.endsWith(DeclarativeRegistry.WEBSCRIPT_DESC_XML)) {
			String id = documentPath.substring(0, documentPath.lastIndexOf(DeclarativeRegistry.WEBSCRIPT_DESC_XML));
			DescriptionImpl desc = webscriptDescriptions.get(documentPath);
			// !!! Fallback for raw annotation solution
			if (null == desc) {
				log.debug("Generating webscript description for: " + documentPath);
				desc = new DescriptionImpl();
				desc.setId(id);
				desc.setShortName("MyWebscript");
				desc.setUris(new String[] {"/mywebscript"});
				desc.setRequiredAuthentication(RequiredAuthentication.user);
			}
			String descText = desc.toString();
			log.debug(documentPath + " >> " + desc.toString());
			return new ByteArrayInputStream(descText.getBytes());
		} else {
			throw new IOException("Can't provide content for: " + documentPath);		
		}
	}

    /**
     * Creates a document.
     *
     * @param documentPath  document path
     * @param content       content of the document to write
     *
     * @throws IOException if the document already exists or the create fails
     */
	@Override
	public void createDocument(String documentPath, String content) throws IOException {
		log.debug("createDocument");
		if (webscriptPaths.contains(documentPath)) {
			throw new IOException("Dac already installed in this store");
		}
		webscriptPaths.add(documentPath);
		descriptorPaths.add(documentPath + DeclarativeRegistry.WEBSCRIPT_DESC_XML);
	}

    /**
     * Updates an existing document.
     *
     * @param documentPath  document path
     * @param content       content to update the document with
     *
     * @throws IOException if the document does not exist or the update fails
     */
	@Override
	public void updateDocument(String documentPath, String content) throws IOException {
		log.debug("updateDocument");
		throw new IOException("No docs in this store yet");
	}

    /**
     * Removes an existing document.
     *
     * @param documentPath  document path
     * @return  whether the operation succeeded
     *
     * @throws IOException if the document does not exist or the remove fails
     */
	@Override
	public boolean removeDocument(String documentPath) throws IOException {
		log.debug("removeDocument");
		if (webscriptPaths.contains(documentPath)) {
			webscriptPaths.remove(documentPath);
			return true;
		} else {
			return false;
		}
	}

    /**
     * Gets the template loader for this store
     *
     * @return  template loader
     */
	@Override
	public TemplateLoader getTemplateLoader() {
		log.debug("getTemplateLoader");
		return new MyWebscriptTemplateLoader();
	}

    /**
     * Gets the script loader for this store
     *
     * @return  script loader
     */
	@Override
	public ScriptLoader getScriptLoader() {
		log.debug("getScriptLoader");
		return new MyWebscriptScriptLoader();
	}

	public class MyWebscriptTemplateLoader implements TemplateLoader {
		@Override
		public void closeTemplateSource(Object arg0) throws IOException {
			log.debug("MyWebscriptTemplateLoader.closeTemplateSource");
		}

		@Override
		public Object findTemplateSource(String arg0) throws IOException {
			log.debug("MyWebscriptTemplateLoader.findTemplateSource: " + arg0);
			return null;
		}

		@Override
		public long getLastModified(Object arg0) {
			log.debug("MyWebscriptTemplateLoader.getLastModified");
			return 0;
		}

		@Override
		public Reader getReader(Object arg0, String arg1) throws IOException {
			log.debug("MyWebscriptTemplateLoader.getReader");
			throw new IOException("No scripts available via this store");
		}
	}
	
	public class MyWebscriptScriptLoader implements ScriptLoader {
		@Override
		public ScriptContent getScript(String path) {
			log.debug("MyWebscriptScriptLoader.getScript");
			return null;
		}
	}

	private ConfigurableApplicationContext applicationContext = null;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		if (applicationContext instanceof ConfigurableApplicationContext) {
			this.applicationContext = (ConfigurableApplicationContext) applicationContext;
		}
	}

}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
