package org.alfresco.demoamp.annotated;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.Description;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

@Webscript
public class MyAnnotatedWebscript {

	// Because I have multiple webscripts defined in this single bean I can have shared state without much effort.
	// Obviously we should be careful to make said shared safe thread-safe.
	private AtomicInteger counter = new AtomicInteger();
	
	// Name automatically set, method automatically set to get, default authentication is none.
	// Note: we can use autowired stuff like nodeService in our handlers.
	@WebscriptHandler(
			urls={"/myannotatedwebscript"}, 
			shortname="My Annotated Webscript")
	public void handler(AbstractWebScript webscript, WebScriptRequest req, WebScriptResponse res) throws IOException {
		res.setStatus(200);
		res.getWriter().write("This is my annotated webscript example... ");
		if (null == nodeService) {
			res.getWriter().write("The node service was NOT injected. ");
		} else {
			res.getWriter().write("The node service was injected. Here is the nodeRef for the root node: " + nodeService.getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE).toString());			
		}
		res.getWriter().write("Count = " + counter.getAndIncrement() + ". ");
	}
	
	// Name automatically set but does not clash because this is for post instead of get.
	// Note: the order of arguments is not important
	@WebscriptHandler(
			urls={"/myotherannotatedwebscript"}, 
			shortname="Other super spiffy webscript", 
			authentication=Description.RequiredAuthentication.user, 
			method="post")
	public void otherHandler(WebScriptResponse res, AbstractWebScript webscript, WebScriptRequest req) throws IOException {
		res.getWriter().write("Other count = " + counter.getAndIncrement() + ". ");		
	}
	
	// This webscript also defines a get handler, because we already have such a handler we need to override the name.
	// Note: the name does not include the method as that is handled internally.
	// !!!! Should probably inject the package into the name if one is not specified to make the annotation shorter for most cases
	// Note: the arity is also not fixed, however you can only have one argument of each of the following types:
	//       AbstractWebScript, WebScriptRequest, WebScriptResponse.
	//       The arguments are optional, but you can't add any other arguments.
	@WebscriptHandler(
			nameoverride="org.alfresco.demoamp.annotated.myannotatedwebscriptextension", 
			urls={"/myotherannotatedwebscriptextension"}, 
			shortname="Extended webscript", 
			authentication=Description.RequiredAuthentication.user, 
			method="get" /* default */)
	public void extendedHandler(WebScriptResponse res) throws IOException {
		res.getWriter().write("Extended count = " + counter.getAndIncrement() + ". ");		
	}
	
	// This webscript contains path and url parameters and dumps some info
	@WebscriptHandler(
			nameoverride="org.alfresco.demoamp.annotated.myannotatedargumentativewebscript", 
			urls={"/myannotatedargumentativewebscript/{greeting}?verbose={verbose?}"}, 
			shortname="Extended webscript", 
			authentication=Description.RequiredAuthentication.user)
	public void argumentativeHandler(AbstractWebScript webscript, WebScriptRequest req, WebScriptResponse res) throws IOException {
		Writer w = res.getWriter();
		String[] parameterNames = req.getParameterNames();
		if (req.getParameterNames().length > 0) {
			w.write("Parameters from request:\n");
			for (String parameterName : parameterNames) {
				w.write("\t" + parameterName + ": " + req.getParameter(parameterName) + "\n");
			}
		} else {
			w.write("No parameters found\n");
		}
		w.write("Path info from request: " + req.getPathInfo() + "\n");
		w.write("Query string from request: " + req.getQueryString() + "\n");
		w.write("Verbosity from request: " + req.getParameter("verbose") + "\n");
		Map<String, String> templateVars = req.getServiceMatch().getTemplateVars();
		if (null != templateVars) {
			w.write("Template variables from request/match:\n");
			for (String var : templateVars.keySet()) {
				w.write("\t" + var + ": " + templateVars.get(var) + "\n");
			}
		}
		res.getWriter().write("Oh, and by the way... count = " + counter.getAndIncrement() + ". ");
	}

	private NodeService nodeService = null;
	@Autowired
	@Qualifier("NodeService")
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
