package org.alfresco.demoamp.annotated;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

@Component("webscript.org.alfresco.demoamp.annotated.mywebscript.get")
public class MyWebscript extends AbstractAnnotatedWebscript
{
	public static final Log log = LogFactory.getLog(MyWebscript.class);
	
	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		log.debug("RUNNING GET WEBSCRIPT");
		res.setStatus(200);
		res.getWriter().write("This is MyWebscript with even more sweet changes");
	}

}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
