/**
 * 
 */
package org.alfresco.demoamp.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.extensions.webscripts.Description;

/**
 * Apply this to a method in a class tagged with @Webscript to 
 * set the method up as a webscript handler.
 * 
 * @author Bindu Wavell <bindu@ziaconsulting.com>
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebscriptHandler {
	String[] urls();
	String shortname();
	String nameoverride() default "";
	String method() default "get";
	String description() default "";
	Description.RequiredAuthentication authentication() default Description.RequiredAuthentication.none;
	String runas() default "";
}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
