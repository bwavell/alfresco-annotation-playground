package org.alfresco.demoamp.annotated;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

public class AnnotatedWebscriptProxy extends AbstractWebScript {

	private Object target;
	private Method method;

	public AnnotatedWebscriptProxy() {
	}
	
	public AnnotatedWebscriptProxy(Object target, Method method) {
		this.setTarget(target);
		this.setMethod(method);
	}
	
	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		List<Object> args = new ArrayList<Object>();
		Class<?>[] parameterTypes = getMethod().getParameterTypes();
		for (Class<?> parameterType : parameterTypes) {
			if (parameterType.isInstance(req)) {
				args.add(req);
			} else if (parameterType.isInstance(res)) {
				args.add(res);
			} else if (parameterType.isAssignableFrom(AbstractWebScript.class)) {
				args.add(this);
			} else {
				throw new IOException("Attempt to call webscript handler method with unknown parameter type: " + parameterType.getCanonicalName());
			}
		}
		try {
			getMethod().invoke(getTarget(), args.toArray());
		} catch (IllegalAccessException e) {
			throw new IOException("Illegal access while attempting to call webscript handler", e);
		} catch (IllegalArgumentException e) {
			throw new IOException("Illegal argument while attempting to call webscript handler", e);
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof IOException) {
				throw (IOException) e.getCause();
			} else {
				throw new IOException("Invocation problem while attempting to call webscript handler", e); 
			}
		}
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
