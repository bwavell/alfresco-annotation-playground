package org.alfresco.demoamp.annotated;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

public abstract class AbstractAnnotatedWebscript extends AbstractWebScript
{
	@Autowired
	@Qualifier("webscripts.mywebscriptstore")
	private MyWebscriptStore myWebscriptStore;
	
	@PostConstruct
	public void beanInit() throws IOException {
		String thisPackage = this.getClass().getPackage().getName();
		String path = thisPackage.replace('.', '/');
		String beanName = this.getClass().getAnnotation(Component.class).value();
		/*
		String thisClass = this.getClass().getSimpleName();
		String expectedBeanPrefix = "webscript." + path + "." + thisClass.toLowerCase();
		if (!beanName.startsWith(expectedBeanPrefix)) {
			beanName = expectedBeanPrefix + ".get";
			this.getClass().getAnnotation(Component.class)
			this.getClass().
		}
		*/
		int pos = beanName.lastIndexOf('.');
		pos = beanName.lastIndexOf('.', pos-1);
		String id = beanName.substring(pos+1);
		myWebscriptStore.createDocument(path + "/" + id, null);
	}
}

/*
 * Copyright 2016 Bindu Wavell <bindu@ziaconsulting.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
